from tkinter import *
from isort import stream
from pytube import YouTube

root = Tk()
root.geometry('500x550')
root.resizable(0,0)
root.title("Youtube Video Downloader")
root.configure(background='white')

#enter link
link = StringVar()

Label(root, text = 'Enter link:', bg = 'white', font = 'arial 12').place(x = 50, y = 30)
link_enter = Entry(root, width = 65, bg = 'white smoke', textvariable = link).place(x = 50, y = 70)

path = StringVar()

Label(root, text = 'Enter path:', bg = 'white', font = 'arial 12').place(x = 50, y = 120)
path_enter = Entry(root, width = 65, bg = 'white smoke', textvariable = path).place(x = 50, y = 160)


#function to download video

def Downloader():
    url =YouTube(str(link.get()))

    if level.get() == 1:
        video = url.streams.get_by_resolution('144p')
    elif level.get() == 2:
        video = url.streams.get_by_resolution('240p')
    elif level.get() == 3:
        video = url.streams.get_by_resolution('360p')
    elif level.get() == 4:
        video = url.streams.get_by_resolution('480p')
    elif level.get() == 5:
        video = url.streams.get_by_resolution('720p')

    video.download(path.get())
    Label(root, text = 'Downloaded', font = 'arial 10', bg = 'white', fg= 'limegreen').place(x= 210 , y = 460)  
    #Label(root, text = url.streams.title(), font = 'arial 10', bg = 'white', fg= 'limegreen').place(x= 210 , y = 510)  

level = IntVar()
Label(root, text = 'Quality:', bg = 'white', font = 'arial 12').place(x = 50, y = 210)
Radiobutton(text='144p', bg = 'white', font = 'arial 12', variable=level, value=1).place(x = 50, y = 240)
Radiobutton(text='240p', bg = 'white', font = 'arial 12', variable=level, value=2).place(x = 50, y = 270)
Radiobutton(text='360p', bg = 'white', font = 'arial 12', variable=level, value=3).place(x = 50, y = 300)
Radiobutton(text='480p', bg = 'white', font = 'arial 12', variable=level, value=4).place(x = 50, y = 330)
Radiobutton(text='720p', bg = 'white', font = 'arial 12', variable=level, value=5).place(x = 50, y = 360)

Button(root,text = 'DOWNLOAD', font = 'arial 15 bold', fg = 'white', bg = 'limegreen', command = Downloader).place(x=180, y = 400)

root.mainloop()

